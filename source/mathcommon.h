#ifndef MATHCOMMON_H
#define MATHCOMMON_H

#include <cmath>
#include <algorithm>

float const FLOAT_EPSILON = 0.00001f;

template<typename T>
inline bool equals(T const first, T const second)
{
    return (first == second);
}

template<>
inline bool equals<float>(float const first, float const second)
{
    return std::abs(first - second) < FLOAT_EPSILON;
}

inline float clamp(float const value, float const from, float const to)
{
    return (value > to ? to : (value < from ? from : value));
}

inline float clamp(float value)
{
    return std::min(std::max(value, 0.0f), 1.0f);
}

#endif // MATHCOMMON_H
