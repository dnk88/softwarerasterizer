#ifndef FIXEDRASTERIZER_H
#define FIXEDRASTERIZER_H

#include <QPainter>
#include <QImage>
#include <QVector2D>
#include <QPoint>
#include "geometry.h"
#include "pixelbuffer.h"
#include "shader.h"

enum class RenderMode {
    WireframeMode,
    SolidMode
};

class FixedRasterizer
{
public:
    FixedRasterizer(PixelBuffer *frameBuffer, DepthBuffer *depthBuffer);
    ~FixedRasterizer();

    void line(int x0, int y0, int x1, int y1, QColor &color);

    void triangle(Vec3i t0, Vec3i t1, Vec3i t2, QColor &color);

    void triangle(Vec3i vert0, Vec3i vert1, Vec3i vert2,
                  Vec3f tex0, Vec3f tex1, Vec3f tex2, QColor &color);

    void triangle(Vec3i vert0, Vec3i vert1, Vec3i vert2,
                  Vec3f tex0, Vec3f tex1, Vec3f tex2,
                  Vec3f norm0, Vec3f norm1, Vec3f norm2,
                  QColor &color);

    void setEnableZBuffer(bool enabled);
    bool enabledZBuffer() const;

    void setRenderMode(RenderMode mode);
    RenderMode renderMode() const;

    Color texture2D(float tu, float tv);
    void setTexture(QImage *image);
private:
    void line_impl(int x0, int y0, int x1, int y1,
                   QColor &color);

    void line_impl(Vec3i t0, Vec3i t1,
                   QColor &color);

    void scanline_impl(Vec3i vert0, Vec3i vert1, Vec3i vert2,
                       Vec3f tex0, Vec3f tex1, Vec3f tex2,
                       Color &color);

    void barycentric_zigzag_impl(Vec3i t0, Vec3i t1, Vec3i t2,
                                 Vec3f tex0, Vec3f tex1, Vec3f tex2,
                                 Vec3f norm0, Vec3f norm1, Vec3f norm2,
                                 Color &color);

    QImage *m_texture;

    PixelBuffer *m_framebuffer;
    DepthBuffer *m_zbuffer;

    QRect m_scissorBox;

    RenderMode m_renderMode;
    bool m_enabledZBuffer;
    bool m_enabledBilinearFilter = false;

    friend class Pipeline;
};


#endif // FIXEDRASTERIZER_H
