#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include "model.h"

#include <QDebug>

Model::Model(const char *filename) : verts_(), texs_(), normals_(), faces_() {
    scale_ = Matrix::identity();
    rotate_ = Matrix::identity();

    std::ifstream in;
    in.open (filename, std::ifstream::in);
    if (in.fail()) return;
    std::string line;
    while (!in.eof()) {
        std::getline(in, line);
        std::istringstream iss(line.c_str());
        char trash;
        if (!line.compare(0, 2, "v ")) {
                    iss >> trash;
                    Vec3f v;
                    for (int i = 0; i < 3; i++) iss >> v[i];
                    verts_.push_back(v);
        } else if (!line.compare(0, 3, "vt ")) {
            iss >> trash;
            iss >> trash;
            Vec3f v;
            for (int i = 0; i < 3; i++) iss >> v[i];

            texs_.push_back(v);
        } else if (!line.compare(0, 3, "vn ")) {
            iss >> trash;
            iss >> trash;
            Vec3f v;
            for (int i = 0; i < 3; i++) iss >> v[i];
            v.normalize();

            normals_.push_back(v);
        } else if (!line.compare(0, 2, "f ")) {
            std::vector<Vec3i> f;
            Vec3i tmp;
            iss >> trash;
            while (iss >> tmp[0] >> trash >> tmp[1] >> trash >> tmp[2]) {
                for (int i=0; i<3; i++) tmp[i]--; // in wavefront obj all indices start at 1, not zero
                f.push_back(tmp);
            }
            faces_.push_back(f);
        }
    }
    std::cerr << "# v# " << verts_.size() << " f# "  << faces_.size() << " vt# " << texs_.size() <<  " vn# " << normals_.size() << std::endl;
}

Model::~Model()
{
}

int Model::nverts() const
{
    return (int)verts_.size();
}

int Model::nfaces() const
{
    return (int)faces_.size();
}

std::vector<int> Model::face(int idx) const
{
    std::vector<int> face;
    for (int i = 0; i < (int)faces_[idx].size(); i++)
        face.push_back(faces_[idx][i][0]);
    return face;
}

Vec3f Model::uv(int iface, int nthvert) const
{
    return texs_[faces_[iface][nthvert][1]];
}

Vec3f Model::normal(int iface, int nthvert) const
{
    return normals_[faces_[iface][nthvert][2]];
}

Vec3f Model::vert(int i) const
{
    return verts_[i];
}

Vec3f Model::tex(int i) const
{
    return texs_[i];
}

Vec3f Model::normal(int i) const
{
    return normals_[i];
}

Matrix Model::transform() const
{
    return matrix_;
}

void Model::scale(Vec3f d)
{
    scale_ = Matrix::identity();
    scale_[0][0] = d.x;
    scale_[1][1] = d.y;
    scale_[2][2] = d.z;

    matrix_ = scale_ * rotate_;
}

void Model::move(Vec3f d)
{

}

void Model::rotate(Vec3f axis, float rad)
{
    Vec3f axis_normalized = axis.normalize();

    float const cos_value = std::cos(rad);
    float const sin_value = std::sin(rad);

    rotate_ = Matrix::identity();

    rotate_[0][0] = axis_normalized.x * axis_normalized.x * (1.0f - cos_value) + cos_value;
    rotate_[0][1] = axis_normalized.x * axis_normalized.y * (1.0f - cos_value) + axis_normalized.z * sin_value;
    rotate_[0][2] = axis_normalized.x * axis_normalized.z * (1.0f - cos_value) - axis_normalized.y * sin_value;

    rotate_[1][0] = axis_normalized.x * axis_normalized.y * (1.0f - cos_value) - axis_normalized.z * sin_value;
    rotate_[1][1] = axis_normalized.y * axis_normalized.y * (1.0f - cos_value) + cos_value;
    rotate_[1][2] = axis_normalized.y * axis_normalized.z * (1.0f - cos_value) + axis_normalized.x * sin_value;

    rotate_[2][0] = axis_normalized.x * axis_normalized.z * (1.0f - cos_value) + axis_normalized.y * sin_value;
    rotate_[2][1] = axis_normalized.y * axis_normalized.z * (1.0f - cos_value) - axis_normalized.x * sin_value;
    rotate_[2][2] = axis_normalized.z * axis_normalized.z * (1.0f - cos_value) + cos_value;

    matrix_ = scale_ * rotate_;
}
