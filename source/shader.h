#ifndef SHADER_H
#define SHADER_H

#include <algorithm>
#include <cfloat>
#include <cmath>
#include <limits>
#include <memory>
#include <vector>

#include <QDebug>
#include <QImage>

#include "mathcommon.h"
#include "geometry.h"
#include "pixelbuffer.h"

typedef QImage SamplerTexture2D;
typedef DepthBuffer SamplerTextureDepth;

class IShader
{
public:
    virtual ~IShader() {}

    virtual void vertex(Vec4f &vertex) = 0;
    virtual bool fragment(Vec4f &color) = 0;
    virtual void setupUniformValues() {}
    virtual std::unique_ptr<IShader> clone() = 0;

    Vec4f position;
    Vec4f normal;
    Vec3f texCoord;
    Vec4f vertexColor;
//    Vec2f multiTexCoord0;

    Vec3f fragNormal;
    Vec4f fragColor;
    Vec3f fragCoord;
    float fragDepth;
    bool frontFacing;

    Matrix projectionMatrix;
    Matrix viewportMatrix;
    Matrix viewMatrix;
    Matrix modelMatrix;
    Matrix modelViewMatrix;
    Matrix modelViewProjectionMatrix;

protected:
    Vec4f texture2D(SamplerTexture2D &texture, float tu, float tv);
    float textureDepth(SamplerTextureDepth &texture, float tu, float tv);

    Vec4f mul(Vec4f a, Vec4f b)
    {
        return Vec4f{a[0] * b[0], a[1] * b[1], a[2] * b[2], a[3] * b[3]};
    }

    Vec4f clamp(Vec4f value)
    {
        return Vec4f{::clamp(value[0]), ::clamp(value[1]), ::clamp(value[2]), ::clamp(value[3])};
    }

    std::vector<float *> varyingFloatAtrribs;
    std::vector<Vec3f *> varyingVec3fAtrribs;

    void clearVarying()
    {
        varyingFloatAtrribs.clear();
        varyingVec3fAtrribs.clear();
    }

    void registerVaryingFloat(float *value)
    {
        varyingFloatAtrribs.push_back(value);
    }

    void registerVaryingVec3f(Vec3f *value)
    {
        varyingVec3fAtrribs.push_back(value);
    }

    bool m_enabledBilinearFilter = false;

    friend class FragmentProcessor;
    friend class Rasterizer;
    friend class Pipeline;
    friend class MainWindow;
};

#endif // SHADER_H
