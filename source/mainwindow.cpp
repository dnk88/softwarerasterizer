#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>
#include <QElapsedTimer>
#include <QDebug>

#include "pipeline.h"
#include "rasterizer.h"
#include "model.h"
#include "shader.h"
#include "lighting.h"

Vec3f lightPos{1.0, 1.5, 1.5};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->paintButton, SIGNAL(clicked()), this, SLOT(paintImage()));
    connect(ui->frameLabel, SIGNAL(clicked()), this, SLOT(paintImage()));

    pipeline = new Pipeline;
    tex = new QImage("african_head_diffuse.png");
    tex2 = new QImage("african_head_nm_tangent.png");
    tex3 = new QImage("floor_diffuse.png");
    model = new Model("african_head.obj");
    floor = new Model("floor.obj");

    shader1 = new LamberLightShader;
    shader2 = new BlinnLightShader;
    shader3 = new BumpMapPhongLightShader;
    shader4 = new DepthShader;
    shader5 = new PhongLightShader;

    //shader->lightPos = lightPos;

    shader1->diffuseMap = *tex3;
    shader2->diffuseMap = *tex;
    shader3->diffuseMap = *tex;
    shader5->diffuseMap = *tex;
    shader3->normalMap = *tex2;

    pipeline->setShader(shader1);
}

MainWindow::~MainWindow()
{
    delete tex;
    delete tex2;
    delete tex3;
    delete model;
    delete floor;
    delete pipeline;
    delete ui;
}

void MainWindow::changeShader()
{
    // TODO: fix me
    if (ui->shaderRadioButton_1->isChecked())
    {
        pipeline->setShader(shader1);
    }
    else if (ui->shaderRadioButton_2->isChecked())
    {
        //shader->lightPos = lightPos;
        pipeline->setShader(shader2);
    }
    else if (ui->shaderRadioButton_3->isChecked())
    {
        pipeline->setShader(shader3);
    }
    else if (ui->shaderRadioButton_4->isChecked())
    {
        pipeline->setShader(shader4);
    }
    else if (ui->shaderRadioButton_5->isChecked())
    {
        pipeline->setShader(shader5);
    }
}

void MainWindow::changeShaderSettings()
{
     // TODO: fix me
    pipeline->m_shader->m_enabledBilinearFilter = ui->texFilterCheckBox->isChecked();
    pipeline->m_rasterizer->setEnableBlending(ui->blendCheckBox->isChecked());
}

void MainWindow::paintImage()
{
    int frameWidth = ui->frameLabel->size().width();
    int frameHeight = ui->frameLabel->size().height();
    if (frameWidth != pipeline->m_width ||
        frameHeight != pipeline->m_height)
    {
        qDebug() << frameWidth << "x" << frameHeight;
        pipeline->resizeFrameBuffer(frameWidth, frameHeight);
    }

    Vec3f       eye(1,1,3);
    Vec3f    center(0,0,0);
    Vec3f        up(0,1,0);

    QElapsedTimer timer;
    timer.start();

    static float angle = 0;
    angle += 0.1;

    model->rotate(Vec3f(0.0f, 1.0f, 0.0f), angle);
    model->scale(Vec3f(ui->scaleSpinBox->value(), ui->scaleSpinBox->value(), ui->scaleSpinBox->value()));
    floor->rotate(Vec3f(0.0f, 1.0f, 0.0f), angle);
    floor->scale(Vec3f(ui->scaleSpinBox->value(), ui->scaleSpinBox->value(), ui->scaleSpinBox->value()));


    pipeline->clearFramebuffer();
    pipeline->camera().setLookat(eye, center, up);

    // Draw floor with first shader
    pipeline->setShader(shader1);
    pipeline->drawModel(*floor);

    // Draw first model
    changeShader();
    pipeline->drawModel(*model);

    // Draw floor with first shader
   // pipeline->setShader(shader1);
   // pipeline->drawModel(*floor);

    qDebug() << "The slow operation took" << timer.elapsed() << "milliseconds";

    PixelBuffer *framebuffer = pipeline->m_frameBuffer;
    QImage image(framebuffer->rawData(), framebuffer->width(), framebuffer->height(), QImage::Format_RGB32);
    ui->frameLabel->setPixmap(QPixmap::fromImage(image));
    ui->centralWidget->update();
}

void MainWindow::paintWireHead()
{
}
