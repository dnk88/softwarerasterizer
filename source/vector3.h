#ifndef VECTOR3_H
#define VECTOR3_H

#include <cmath>
//#include <xmmintrin.h>
//#include <immintrin.h>

/** 3-dimensional vector template class */
template<typename T>
class vec3 final
{
public:
    /** X-coordinate of vector */
    T x;

    /** Y-coordinate of vector */
    T y;

    /** Z-coordinate of vector */
    T z;

    /** Default constructor (initializes with default values) */
    vec3();

    /** Constructs vector with specified values
    * @param x X-coordinate of a vector
    * @param y Y-coordinate of a vector
    * @param z Z-coordinate of a vector
    */
    vec3(T const x, T const y, T const z);

    // Operators
    //

    bool operator==(vec3<T> const& v) const;
    vec3<T> operator+(vec3<T> const& v) const;
    vec3<T>& operator+=(vec3<T> const& v);
    vec3<T> operator-(vec3<T> const& v) const;
    vec3<T>& operator-=(vec3<T> const& v);
    vec3<T> operator-() const;
    vec3<T> operator*(float const f) const;
    vec3<T>& operator*=(float const f);
    vec3<T> operator/(float const f) const;
    vec3<T>& operator/=(float const f);

    /** Calculates vector's length
    * @returns Length
    */
    float length() const;

    /** Calculates vector's squared length, thus avoiding square root operation
    * @returns Squared length
    */
    float length_sqr() const;

    /** Normalizes vector (changing length to 1) */
    void normalize();

    /** Gets normalized vector, leaving this vector untouched
    * @returns Normalized version of this vector
    */
    vec3<T> normalized() const;

    /** Calculates dot product with another vector
    * @returns Dot product
    */
    float dot(vec3<T> const& v) const;

    /** Calculates cross product with another vector
    * @returns Cross product
    */
    vec3 cross(vec3<T> const& v) const;

    /** Calculates distance to another point
    * @returns Distance
    */
    float distance_to(vec3<T> const& p) const;

    /** Calculates angle with another vector
    * @returns Angle in radians
    */
    float angle_with(vec3<T> const& v) const;

    /** Calculates projection of this vector on another vector
    * @returns Projection vector
    */
    vec3<T> projection_on(vec3<T> const& v) const;

    /** Calculates perpendicular from this vector to another vector
    * @returns Perpendicular
    */
    vec3<T> perpendicular_on(vec3 const& v) const;
};

// vec3<T> implementation
//

template<typename T>
inline vec3<T>::vec3()
    : x{}, y{}, z{}
{

}

template<typename T>
inline vec3<T>::vec3(T const x, T const y, T const z)
    : x(x), y(y), z(z)
{

}

template<typename T>
inline bool vec3<T>::operator==(vec3<T> const& v) const
{
    return equals(this->x, v.x) && equals(this->y, v.y) && equals(this->z, v.z);
}

template<typename T>
inline vec3<T> vec3<T>::operator+(vec3<T> const& v) const
{
    return vec3<T>{this->x + v.x, this->y + v.y, this->z + v.z};
}

template<typename T>
inline vec3<T>& vec3<T>::operator+=(vec3<T> const& v)
{
    this->x += v.x;
    this->y += v.y;
    this->z += v.z;

    return *this;
}

template<typename T>
inline vec3<T> vec3<T>::operator-(vec3<T> const& v) const
{
    return vec3{this->x - v.x, this->y - v.y, this->z - v.z};
}

template<typename T>
inline vec3<T>& vec3<T>::operator-=(vec3<T> const& v)
{
    this->x -= v.x;
    this->y -= v.y;
    this->z -= v.z;

    return *this;
}

template<typename T>
inline vec3<T> vec3<T>::operator-() const
{
    return vec3<T>{-this->x, -this->y, -this->z};
}

template<typename T>
inline vec3<T> vec3<T>::operator*(float const f) const
{
    return vec3<T>{this->x * f, this->y * f, this->z * f};
}

template<typename T>
inline vec3<T>& vec3<T>::operator*=(float const f)
{
    this->x *= f;
    this->y *= f;
    this->z *= f;

    return *this;
}

template<typename T>
inline vec3<T> vec3<T>::operator/(float const f) const
{
    return vec3{this->x / f, this->y / f, this->z / f};
}

template<typename T>
inline vec3<T>& vec3<T>::operator/=(float const f)
{
    this->x /= f;
    this->y /= f;
    this->z /= f;

    return *this;
}

template<typename T>
inline float vec3<T>::length() const
{
    return std::sqrt(this->x * this->x + this->y * this->y + this->z * this->z);
}

template<typename T>
inline float vec3<T>::length_sqr() const
{
    return this->x * this->x + this->y * this->y + this->z * this->z;
}

template<typename T>
inline void vec3<T>::normalize()
{
    float const length_reciprocal{1.0f / this->length()};

    this->x *= length_reciprocal;
    this->y *= length_reciprocal;
    this->z *= length_reciprocal;
}

template<typename T>
inline vec3<T> vec3<T>::normalized() const
{
    float const length_reciprocal{1.0f / this->length()};

    return vec3<T>{this->x * length_reciprocal, this->y * length_reciprocal, this->z * length_reciprocal};
}

template<typename T>
inline float vec3<T>::distance_to(vec3<T> const& v) const
{
    float const dist_x{v.x - this->x};
    float const dist_y{v.y - this->y};
    float const dist_z{v.z - this->z};

    return std::sqrt(dist_x * dist_x + dist_y * dist_y + dist_z * dist_z);
}

template<typename T>
inline float vec3<T>::angle_with(vec3<T> const& v) const
{
    return std::acos(this->dot(v) / (this->length() * v.length()));
}

template<typename T>
inline vec3<T> vec3<T>::projection_on(vec3<T> const& v) const
{
    return v.normalized() * (this->dot(v) / v.length());
}

template<typename T>
inline vec3<T> vec3<T>::perpendicular_on(vec3<T> const& v) const
{
    return (*this) - this->projection_on(v);
}

template<typename T>
inline float vec3<T>::dot(vec3<T> const& v) const
{
    return this->x * v.x + this->y * v.y + this->z * v.z;
}

template<typename T>
inline vec3<T> vec3<T>::cross(vec3<T> const& v) const
{
    return vec3<T>{this->y * v.z - this->z * v.y, this->z * v.x - this->x * v.z, this->x * v.y - this->y * v.x};
}

template<typename T>
inline vec3<T> operator*(float const f, vec3<T> const& v)
{
    return vec3<T>{v.x * f, v.y * f, v.z * f};
}

// Typedefs
//

using vec3f = vec3<float>;

#endif // VECTOR3_H
