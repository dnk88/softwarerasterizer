#include "pixelbuffer.h"

const ColorRGBA32 ColorRGBA32::BLACK = ColorRGBA32{0, 0, 0, 255};
const ColorRGBA32 ColorRGBA32::WHITE = ColorRGBA32{255, 255, 255, 255};
const ColorRGBA32 ColorRGBA32::RED   = ColorRGBA32{255, 0, 0, 255};
const ColorRGBA32 ColorRGBA32::GREEN = ColorRGBA32{0, 255, 0, 255};
const ColorRGBA32 ColorRGBA32::BLUE  = ColorRGBA32{0, 0, 255, 255};

Color QColorToColor(QColor c)
{
    return Color{static_cast<unsigned char>(c.blue()),
                 static_cast<unsigned char>(c.green()),
                 static_cast<unsigned char>(c.red()),
                 255};
}

Color Vec4fToColor(Vec4f v)
{
    return Color{static_cast<unsigned char>(v[0] * 255),
                 static_cast<unsigned char>(v[1] * 255),
                 static_cast<unsigned char>(v[2] * 255),
                 static_cast<unsigned char>(v[3] * 255)};
}

Vec4f ColorToVec4f(Color c)
{
    return Vec4f{c.r / 255.0f, c.g / 255.0f, c.b / 255.0f, c.a / 255.0f};
}
