#include "lighting.h"

void ColorShader::vertex(Vec4f &vertex)
{
    position = modelViewProjectionMatrix * vertex;
}

bool ColorShader::fragment(Vec4f &color)
{
    fragColor = color;
    return true;
}

void LamberLightShader::vertex(Vec4f &vertex)
{
    position = modelViewProjectionMatrix * vertex;
    normal = modelViewMatrix * normal;

    Vec4f p = modelMatrix * vertex;
     l = (lightPos - proj<3>(p)).normalize();
}

bool LamberLightShader::fragment(Vec4f &color)
{
    // Lambert lighting
    float intensity = ::clamp(fragNormal * l.normalize());

    // Wrap-around lighting
    //const float factor = 0.5f;
    //float intensity = clamp((lightPos * fragNormal ) + factor) / (1.0f + factor);
/*
    if (intensity>.85) intensity = 1;
           else if (intensity>.60) intensity = .80;
           else if (intensity>.45) intensity = .60;
           else if (intensity>.30) intensity = .45;
           else if (intensity>.15) intensity = .30;
           else intensity = 0;
*/
    fragColor = color * intensity;
    fragColor = mul(texture2D(diffuseMap, texCoord.x, texCoord.y) ,fragColor);
    fragColor[3] = 1.0f;
    return true;
}

void BlinnLightShader::vertex(Vec4f &vertex)
{
    position = modelViewProjectionMatrix * vertex;

    Vec4f p = modelViewMatrix * vertex;           // transformed point to world space
    l = (lightPos - proj<3>(p)).normalize();      // vector to light source

    normal = modelMatrix * normal;
}

bool BlinnLightShader::fragment(Vec4f &color)
{
    //const Vec4f diffColor {0.0f, 0.0f, 0.5f, 1.0f };
    const Vec4f specColor {0.6f, 0.6f, 0.6f, 1.0f };

    const float shininess = 80.0;

    Vec3f n = fragNormal.normalize();
    l = l.normalize();

   // Blinn
   Vec3f h = (l + n).normalize();

   //Vec4f diff = color * std::max(n * l, 0.0f);
   Vec4f diff = texture2D(diffuseMap, texCoord.x, texCoord.y) * std::max(n * l, 0.0f);
   Vec4f spec = specColor * std::pow(std::max(n * h, 0.0f), shininess);

   fragColor = diff + spec;
   fragColor[3] = 0.7f;
   fragColor = clamp(fragColor);
    return true;
}

void PhongLightShader::vertex(Vec4f &vertex)
{
    position = modelViewProjectionMatrix * vertex;

    Vec4f p = modelViewMatrix * vertex;           // transformed point to world space
    l = (lightPos - proj<3>(p)).normalize();      // vector to light source

    normal = modelMatrix * normal;
}

bool PhongLightShader::fragment(Vec4f &color)
{
    //const Vec4f diffColor {0.0f, 0.0f, 0.5f, 1.0f };
    const Vec4f specColor {0.6f, 0.6f, 0.6f, 1.0f };

    const float shininess = 80.0;

    Vec3f n = fragNormal.normalize();
    l = l.normalize();

   // Phong
   Vec3f r = (n * (n * l * 2.f) - l).normalize();   // reflected light

   //Vec4f diff = color * std::max(n * l, 0.0f);
   Vec4f diff = texture2D(diffuseMap, texCoord.x, texCoord.y) * std::max(n * l, 0.0f);
   Vec4f spec = specColor * std::pow(std::max(n * r, 0.0f), shininess);

   fragColor = diff + spec;
   fragColor[3] = 0.7f;
   fragColor = clamp(fragColor);
    return true;
}

void BumpMapPhongLightShader::setupUniformValues()
{
}

void BumpMapPhongLightShader::vertex(Vec4f &vertex)
{
    position = modelViewProjectionMatrix * vertex;

    Vec4f p = modelViewMatrix * vertex;           // transformed point to world space
    l = (lightPos - (proj<3>(p))).normalize();                    // vector to light source

    normal = modelMatrix * normal;
}

bool BumpMapPhongLightShader::fragment(Vec4f &color)
{
    const Vec4f diffColor {0.0f, 1.0f, 0.5f, 1.0f };
    const Vec4f specColor {0.3f, 0.3f, 0.3f, 1.0f };
    //const Vec4f specColor {1.0f, 1.0f, 1.0f, 1.0f };
    const float shininess = 20.0;

    Vec3f n = fragNormal.normalize();
    l = l.normalize();

    Vec3f bump = (proj<3>(texture2D(normalMap, texCoord.x, texCoord.y)) * 2.0f - 1.0f);
    n = (n + bump).normalize();

    // Phong
    Vec3f r = (n * (n * l * 2.f) - l);   // reflected light

    //Vec4f diff = color * (n * l);
    Vec4f diff = texture2D(diffuseMap, texCoord.x, texCoord.y) * (n * l) * ::clamp(fragNormal.normalize() * l);
    Vec4f spec = specColor * std::pow(std::max(r.x, 0.0f), shininess);

    fragColor = (diff + spec);
    fragColor = clamp(fragColor);

    fragColor[3] = 0.9f;
    //fragColor = clamp(embed<4>(h));
    return true;
}

void DepthShader::vertex(Vec4f &vertex)
{
    position = modelViewProjectionMatrix * vertex;
}

bool DepthShader::fragment(Vec4f &color)
{
    // Render depth texture
    float intensity = ::clamp(fragDepth);
    fragColor = color * intensity;
    fragColor[3] = 1.0f;
    return true;
}
