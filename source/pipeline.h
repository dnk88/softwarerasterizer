#ifndef PIPELINE_H
#define PIPELINE_H

#include "pixelbuffer.h"
#include "rasterizer.h"
#include "camera.h"
#include "vertexbuffer.h"
#include "geometry.h"
#include "model.h"

class Pipeline
{
public:
    Pipeline();
    ~Pipeline();

    void drawModel(const Model &model);
    void setShader(IShader *shader);

    void resizeFrameBuffer(int width, int height);
    void clearFramebuffer();

    Camera& camera() { return m_camera; }

//private:
    void setupShaderMatrixs(const Matrix &modelMatrix);
    void resizeVaryingAttribs(TrianglePrimitive &primitive);
    void createFrameBuffer(int width, int height);

    int m_width, m_height;

    IShader *m_shader;

    PixelBuffer *m_frameBuffer;
    DepthBuffer *m_depthBuffer;
    Rasterizer *m_rasterizer;
    Camera m_camera;

    Matrix m_modelview;
    Matrix m_projection;
    Matrix m_viewport;
};

#endif // PIPELINE_H
