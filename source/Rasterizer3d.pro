#-------------------------------------------------
#
# Project created by QtCreator 2015-02-01T00:43:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Rasterizer3d
TEMPLATE = app
CONFIG += c++11
QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    rasterizer.cpp \
    model.cpp \
    geometry.cpp \
    pipeline.cpp \
    camera.cpp \
    pixelbuffer.cpp \
    vertexbuffer.cpp \
    framebuffer.cpp \
    texture.cpp \
    shader.cpp \
    fixedrasterizer.cpp \
    vector3.cpp \
    vector4.cpp \
    matrix3x3.cpp \
    matrix4x4.cpp \
    lighting.cpp

HEADERS  += mainwindow.h \
    rasterizer.h \
    geometry.h \
    model.h \
    pipeline.h \
    camera.h \
    pixelbuffer.h \
    vertexbuffer.h \
    framebuffer.h \
    texture.h \
    shader.h \
    fixedrasterizer.h \
    vector3.h \
    vector4.h \
    mathcommon.h \
    matrix3x3.h \
    matrix4x4.h \
    lighting.h

FORMS    += mainwindow.ui
