#ifndef RASTERIZER_H
#define RASTERIZER_H

#include <QPainter>
#include <QImage>
#include <QVector2D>
#include <QPoint>
#include "geometry.h"
#include "pixelbuffer.h"
#include "shader.h"

enum class RenderMode {
    WireframeMode,
    SolidMode
};

struct TrianglePrimitive
{
    Vec4f color[3];
    Vec3f vert[3];
    Vec3f normal[3];
    Vec2f tex[3];

    // TODO: replace to use Vec4f vert[3];
    float w[3];

    std::vector<std::vector<float>> varyingFloatAtrribs;
    std::vector<std::vector<Vec3f>> varyingVec3fAtrribs;
};

class Rasterizer
{
public:
    Rasterizer(PixelBuffer *frameBuffer, DepthBuffer *depthBuffer);
    ~Rasterizer();

    void triangle(const TrianglePrimitive &tri, IShader &shader);

    void setEnableZBuffer(bool enabled);
    bool enabledZBuffer() const;

    void setEnableBlending(bool enabled);
    bool enabledBlending() const;

    void setRenderMode(RenderMode mode);
    RenderMode renderMode() const;

    void setFrameBuffer(PixelBuffer *frameBuffer, DepthBuffer *depthBuffer);

    DepthBuffer *depthBuffer();

private:
    void InterpolateVaryingAttribs(Vec3f bcScreen, const TrianglePrimitive &tri, IShader &shader);
    Vec4f blend(Vec4f src, Vec4f dst, int x, int y, int z, bool useSrc);
    void processFragment(Vec4f fragColor, IShader &shader);

    PixelBuffer *m_framebuffer;
    DepthBuffer *m_zbuffer;

    QRect m_scissorBox;

    RenderMode m_renderMode;
    bool m_enabledZBuffer;
    bool m_backCulling;
    bool m_blending;

    friend class Pipeline;
};

#endif // RASTERIZER_H
