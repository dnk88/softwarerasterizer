#ifndef __MODEL_H__
#define __MODEL_H__

#include <vector>
#include "geometry.h"

class Model {
private:
    std::vector<Vec3f> verts_;
    std::vector<Vec3f> texs_;
    std::vector<Vec3f> normals_;
    std::vector<std::vector<Vec3i> > faces_; // attention, this Vec3i means vertex/uv/normal
    Matrix matrix_;
    Matrix scale_;
    Matrix rotate_;

public:
    Model(const char *filename);
    ~Model();
    int nverts() const;
    int nfaces() const;
    Vec3f vert(int i) const;
    Vec3f tex(int i) const;
    Vec3f uv(int iface, int nthvert) const;
    Vec3f normal(int i) const;
    Vec3f normal(int iface, int nthvert) const;
    std::vector<int> face(int idx) const;

    Matrix transform() const;

    void scale(Vec3f d);
    void move(Vec3f d);
    void rotate(Vec3f axis, float rad);
};

#endif //__MODEL_H__
