#include "vector4.h"

vec4f::vec4f()
    : x{0.0f}, y{0.0f}, z{0.0f}, w{0.0f}
{
}

vec4f::vec4f(float const x, float const y, float const z, float const w)
    : x{x}, y{y}, z{z}, w{w}
{

}
