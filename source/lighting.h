#ifndef LIGHTING_H
#define LIGHTING_H

#include "mathcommon.h"
#include "geometry.h"
#include "pixelbuffer.h"
#include "shader.h"

class ColorShader : public IShader
{
public:
    void vertex(Vec4f &vertex) override;
    bool fragment(Vec4f &color) override;
    std::unique_ptr<IShader> clone() override { return std::unique_ptr<ColorShader>(new ColorShader(*this)); }
};

class LamberLightShader : public IShader
{
public:
    Vec3f l;
    Vec3f lightPos {1.0, 0.5, 1.5};
    SamplerTexture2D diffuseMap;

public:
    LamberLightShader()
    {
        registerVaryingVec3f(&l);
    }

    LamberLightShader(const LamberLightShader& other) : IShader(other)
    {
        // copy data
        l = other.l;
        lightPos =other.lightPos;
        diffuseMap = other.diffuseMap;

        // re-init varying
        clearVarying();
        registerVaryingVec3f(&l);
    }

    void vertex(Vec4f &vertex) override;
    bool fragment(Vec4f &color) override;
    std::unique_ptr<IShader> clone() override { return std::unique_ptr<LamberLightShader>(new LamberLightShader(*this)); }
};

class BlinnLightShader : public IShader
{
public:
    Vec3f l;
    Vec3f lightPos {1.0, 0.5, 1.5};
    Vec3f eyePos {1.0, 1.0, 3.0};
    SamplerTexture2D diffuseMap;

public:
    BlinnLightShader()
    {
        registerVaryingVec3f(&l);
    }

    BlinnLightShader(const BlinnLightShader& other) : IShader(other)
    {
        // copy data
        l = other.l;
        lightPos =other.lightPos;
        eyePos = other.eyePos;
        diffuseMap = other.diffuseMap;

        // re-init varying
        clearVarying();
        registerVaryingVec3f(&l);
    }

    void vertex(Vec4f &vertex) override;
    bool fragment(Vec4f &color) override;
    std::unique_ptr<IShader> clone() override { return std::unique_ptr<BlinnLightShader>(new BlinnLightShader(*this)); }
};

class PhongLightShader : public IShader
{
public:
    Vec3f l;
    Vec3f lightPos {1.0, 0.5, 1.5};
    Vec3f eyePos {1.0, 1.0, 3.0};
    SamplerTexture2D diffuseMap;

public:
    PhongLightShader()
    {
        registerVaryingVec3f(&l);
    }

    PhongLightShader(const PhongLightShader& other) : IShader(other)
    {
        // copy data
        l = other.l;
        lightPos =other.lightPos;
        eyePos = other.eyePos;
        diffuseMap = other.diffuseMap;

        // re-init varying
        clearVarying();
        registerVaryingVec3f(&l);
    }

    void vertex(Vec4f &vertex) override;
    bool fragment(Vec4f &color) override;
    std::unique_ptr<IShader> clone() override { return std::unique_ptr<PhongLightShader>(new PhongLightShader(*this)); }
};

class BumpMapPhongLightShader : public IShader
{
public:
    Vec3f l;
    Vec3f lightPos{1.0, 0.5, 1.5};
    Vec3f eyePos{1.0, 1.0, 3.0};

    SamplerTexture2D diffuseMap;
    SamplerTexture2D normalMap;

public:
    BumpMapPhongLightShader()
    {
        registerVaryingVec3f(&l);
    }

    BumpMapPhongLightShader(const BumpMapPhongLightShader& other) : IShader(other)
    {
        // copy data
        l = other.l;
        lightPos =other.lightPos;
        eyePos = other.eyePos;
        diffuseMap = other.diffuseMap;
        normalMap = other.normalMap;

        // re-init varying
        clearVarying();
        registerVaryingVec3f(&l);
    }

    void vertex(Vec4f &vertex) override;
    bool fragment(Vec4f &color) override;
    void setupUniformValues() override;
    std::unique_ptr<IShader> clone() override { return std::unique_ptr<BumpMapPhongLightShader>(new BumpMapPhongLightShader(*this)); }
};

class DepthShader : public IShader
{
public:
    void vertex(Vec4f &vertex) override;
    bool fragment(Vec4f &color) override;
    std::unique_ptr<IShader> clone() override { return std::unique_ptr<DepthShader>(new DepthShader(*this)); }
};


#endif // LIGHTING_H
