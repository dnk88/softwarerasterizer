#include "fixedrasterizer.h"

#include <QDebug>
#include <QApplication>
#include <QRgb>
#include <algorithm>
#include <vector>
#include <cmath>
#include <limits>
#include <cfloat>

static Vec3f light_dir(1.0, 0.0, 2.0);

FixedRasterizer::FixedRasterizer(PixelBuffer *frameBuffer, DepthBuffer *depthBuffer):
    m_texture(0),
    m_framebuffer(frameBuffer),
    m_zbuffer(depthBuffer),
    m_scissorBox(0, 0, 0, 0),
    m_renderMode(RenderMode::SolidMode),
    m_enabledZBuffer(false)
{
    assert(m_framebuffer);
    assert(m_zbuffer);

    m_scissorBox.setWidth(m_framebuffer->width());
    m_scissorBox.setHeight(m_framebuffer->height());
}

FixedRasterizer::~FixedRasterizer()
{
}

void FixedRasterizer::setTexture(QImage *image)
{
    m_texture = image;
}

void FixedRasterizer::setEnableZBuffer(bool enabled)
{
    m_enabledZBuffer = enabled;
}

bool FixedRasterizer::enabledZBuffer() const
{
    return m_enabledZBuffer;
}

void FixedRasterizer::setRenderMode(RenderMode mode)
{
    m_renderMode = mode;
}

RenderMode FixedRasterizer::renderMode() const
{
    return m_renderMode;
}

void FixedRasterizer::line(int x0, int y0, int x1, int y1, QColor &color)
{
    line_impl(x0, y0, x1, y1, color);
}

Color FixedRasterizer::texture2D(float tu, float tv)
{
    QImage *texture;
    texture = m_texture;

    if (texture != 0) {
        // Clamp
        tu = std::max(0.0f, std::min(tu, 1.0f));
        tv = std::max(0.0f, std::min(tv, 1.0f));

        if (m_enabledBilinearFilter) {
            tu *= (texture->width() - 1);
            tv *= (texture->height() - 1);
            int x = floor(tu);
            int y = floor(tv);

            float u_ratio = tu - x;
            float v_ratio = tv - y;
            float u_opposite = 1 - u_ratio;
            float v_opposite = 1 - v_ratio;

            Color tex_xy = QColorToColor(texture->pixel(x, y));
            Color tex_x1y = QColorToColor(texture->pixel(x + 1, y));
            Color tex_xy1 = QColorToColor(texture->pixel(x, y + 1));
            Color tex_x1y1 = QColorToColor(texture->pixel(x + 1, y + 1));

            Color result = (tex_xy * u_opposite + tex_x1y * u_ratio) * v_opposite +
                           (tex_xy1 * u_opposite + tex_x1y1 * u_ratio) * v_ratio;

            return result;
        } else {
            int u = tu * (texture->width() - 1);
            int v = tv * (texture->height() - 1);

            QRgb p = texture->pixel(u, v);
            return Color{static_cast<unsigned char>(qBlue(p)),
                         static_cast<unsigned char>(qGreen(p)),
                         static_cast<unsigned char>(qRed(p)),
                         255};
        }
    } else {
        return Color{255, 255, 255, 255};
    }
}

void FixedRasterizer::triangle(Vec3i t0, Vec3i t1, Vec3i t2, QColor &color)
{

    /*    line_impl(t0, t1, color);
        line_impl(t1, t2, color);
        line_impl(t0, t2, color);
    */
    //Color c = QColorToColor(color);
        //scanline_impl(t0, t1, t2, c);
        //triangle_impl(t0, t1, t2, color);
        //barycentric_impl(t0, t1, t2, color);
        //barycentric_zigzag_impl(t0, t1, t2, color);
}

void FixedRasterizer::triangle(Vec3i vert0, Vec3i vert1, Vec3i vert2, Vec3f tex0, Vec3f tex1, Vec3f tex2, QColor &color)
{
    Color c = QColorToColor(color);
    scanline_impl(vert0, vert1, vert2,
                  tex0, tex1, tex2, c);
}

void FixedRasterizer::triangle(Vec3i vert0, Vec3i vert1, Vec3i vert2,
              Vec3f tex0, Vec3f tex1, Vec3f tex2,
              Vec3f norm0, Vec3f norm1, Vec3f norm2,
              QColor &color)
{
    if (renderMode() == RenderMode::WireframeMode ) {
        line_impl(vert0, vert1, color);
        line_impl(vert1, vert2, color);
        line_impl(vert0, vert2, color);
    } else {
        Color c = QColorToColor(color);
        scanline_impl(vert0, vert1, vert2,
                      tex0, tex1, tex2, c);
   // barycentric_zigzag_impl(vert0, vert1, vert2,
   //                         tex0, tex1, tex2,
   //                         norm0, norm1, norm2,
   //                         c);
    }
}

static float interpolate(float min, float max, float coefficient)
{
    return min + coefficient * (max - min);
}

void FixedRasterizer::line_impl(Vec3i t0, Vec3i t1, QColor &color)
{
    line_impl(t0.x, t0.y, t1.x, t1.y, color);
}

void FixedRasterizer::line_impl(int x0, int y0, int x1, int y1, QColor &color)
{
    int deltax = abs(x1 - x0);
    int deltay = abs(y1 - y0);
    int error = 0;

    Color c = QColorToColor(color);

    // Проверяем рост отрезка по оси икс и по оси игрек
    if (deltax >= deltay) {
        // Если линия растёт не слева направо, то меняем начало и конец отрезка местами
        if (x0 > x1) {
            std::swap(x0, x1);
            std::swap(y0, y1);
        }

        // Выбираем направление роста координаты y
        int step = (y1 >= y0) ? 1 : -1;
        int deltaerr = deltay;
        int y = y0;

        for (int x = x0; x < x1; x++) {
            if (x >= 0 && y >=0 && x < m_framebuffer->width() && y < m_framebuffer->height())
                m_framebuffer->setPixel(x, m_framebuffer->height() - y - 1, c);
            error += deltaerr;
            if (2 * error >= deltax) {
                 y += step;
                 error -= deltax;
            }
        }
    } else {
        // Если линия растёт не слева направо, то меняем начало и конец отрезка местами
        if (y0 > y1) {
            std::swap(y0, y1);
            std::swap(x0, x1);
        }

        // Выбираем направление роста координаты x
        int step = (x1 >= x0) ? 1 : -1;
        int deltaerr = deltax;
        int x = x0;

        for (int y = y0; y < y1; y++) {
            if (x >= 0 && y >=0 && x < m_framebuffer->width() && y < m_framebuffer->height())
                m_framebuffer->setPixel(x, m_framebuffer->height() - y - 1, c);
            error += deltaerr;
            if (2 * error >= deltay) {
                 x += step;
                 error -= deltay;
            }
        }
    }
}

void FixedRasterizer::scanline_impl(Vec3i vert0, Vec3i vert1, Vec3i vert2,
                   Vec3f tex0, Vec3f tex1, Vec3f tex2,
                   Color &color)
{
    if (vert0.y == vert1.y && vert0.y == vert2.y)
        return; // Вырожденный треугольник

    // Сортируем вершины по y
    if (vert0.y > vert1.y) {
        std::swap(vert0, vert1);
        std::swap(tex0, tex1);
    }
    if (vert0.y > vert2.y) {
        std::swap(vert0, vert2);
        std::swap(tex0, tex2);
    }
    if (vert1.y > vert2.y) {
        std::swap(vert1, vert2);
        std::swap(tex1, tex2);
    }

    // Вычисляем высоту самой длинной стороны
    float total_height = vert2.y - vert0.y;

    for (int y = vert0.y; y < vert2.y; y++ )
    {
        float coefficient_ex = 1;
        float coefficient_sx = 1;

        // Определяем какой сегмент рисуем
        bool half_segment = (y > vert1.y);

        // Вычисляем высоту сегмента треугольника
        float segment_height = half_segment ? (vert2.y - vert1.y) : (vert1.y - vert0.y);

        // Вычисляем коэффициенты[0..1] для последующей интерполяций ex, sx
        if (segment_height)
            coefficient_ex = (half_segment ? (y - vert1.y) : (y - vert0.y)) / segment_height;
        if (total_height)
            coefficient_sx = (y - vert0.y) / total_height;

        float ex, ez, sx, sz;
        float eu, ev, su, sv;

        // Вычисляем текущий scanline от ex до sx, в том числе z координаты и текстурные координаты
        if (half_segment) {
            ex = interpolate(vert1.x, vert2.x, coefficient_ex);
            ez = interpolate(vert1.z, vert2.z, coefficient_ex);

            eu = interpolate(tex1.x / ez, tex2.x / ez, coefficient_ex);
            ev = interpolate(tex1.y / ez, tex2.y / ez, coefficient_ex);
        } else {
            ex = interpolate(vert0.x, vert1.x, coefficient_ex);
            ez = interpolate(vert0.z, vert1.z, coefficient_ex);

            eu = interpolate(tex0.x / ez, tex1.x / ez, coefficient_ex);
            ev = interpolate(tex0.y / ez, tex1.y / ez, coefficient_ex);
        }
        sx = interpolate(vert0.x, vert2.x, coefficient_sx);
        sz = interpolate(vert0.z, vert2.z, coefficient_sx);

        su = interpolate(tex0.x / sz, tex2.x / sz, coefficient_sx);
        sv = interpolate(tex0.y / sz, tex2.y / sz, coefficient_sx);

        // Рисуем от меньшего к большему значению x
        int start_x = sx, end_x = ex;
        int start_z = sz, end_z = ez;

        float start_u = su, end_u = eu;
        float start_v = sv, end_v = ev;
        if (sx > ex)
        {
            start_x = ex; end_x = sx;
            start_z = ez, end_z = sz;

            start_u = eu, end_u = su;
            start_v = ev, end_v = sv;
        }

        // Рисуем текущий scanline
        for (int x = start_x; x < end_x; x++) {
            if (m_enabledZBuffer)
            {
                // Интерполируем z координату
                float coefficient_z = float(x - start_x) / (end_x - start_x);
                float z = interpolate(start_z, end_z, coefficient_z);

                if ((x < 0) || (y < 0) || (x >= m_framebuffer->width()) || (y >= m_framebuffer->height()))
                    continue;

                // Проверяем значение в z buffer
                if (m_zbuffer->pixel(x, y) < z) {
                    m_zbuffer->setPixel(x, y, z);

                    // Интерполируем текстурные координаты
                    float u = interpolate(start_u, end_u, coefficient_z) * (1.0f / z);
                    float v = interpolate(start_v, end_v, coefficient_z) * (1.0f / z);

                    Color c = texture2D(u, v);
                    c = c * color / 255.0f;
                    m_framebuffer->setPixel(x, m_framebuffer->height() - y - 1, c);
                }
            } else {
                m_framebuffer->setPixel(x, m_framebuffer->height() - y - 1, color);
            }
        }
    }
}

void FixedRasterizer::barycentric_zigzag_impl(Vec3i t0, Vec3i t1, Vec3i t2,
                                         Vec3f tex0, Vec3f tex1, Vec3f tex2,
                                         Vec3f norm0, Vec3f norm1, Vec3f norm2,
                                         Color &color)
{
    if (t0.y == t1.y && t0.y == t2.y)
        return; // i dont care about degenerate triangles

    int maxX = std::max(t0.x, std::max(t1.x, t2.x));
    int minX = std::min(t0.x, std::min(t1.x, t2.x));
    int maxY = std::max(t0.y, std::max(t1.y, t2.y));
    int minY = std::min(t0.y, std::min(t1.y, t2.y));

    // spanning vectors of edge (v1,v2) and (v1,v3)
    Vec2i vs1(t1.x - t0.x, t1.y - t0.y);
    Vec2i vs2(t2.x - t0.x, t2.y - t0.y);

    //  Модуль векторного произведения двух краев треугольника (определитель матрицы)
    float cs_vs12 = dot(vs1, vs2);
    if (cs_vs12 <= 1) {
        // back side triangle skip
        return;
    }

    int stepX = 1;
    int x = minX;
    int y = minY;

    static long checkPixels = 0;
    bool notPainted = true;
    while ((y >= minY) && (y <= maxY))
    {
        notPainted = true;
        while ((x >= minX) && (x <= maxX))
        {
            if ((x < 0) || (y < 0) || (x >= m_framebuffer->width()) || (y >= m_framebuffer->height()))
            {
                x = x + stepX;
                continue;
            }

            // Определяем находится ли точка внутри треугольника
            Vec2i q(x - t0.x, y - t0.y);

            float s = dot(q, vs2) / cs_vs12;
            float t = dot(vs1, q) / cs_vs12;
            float st = 1.f - (s + t);

            // inside triangle
            if ( (s >= 0) && (t >= 0) && (st >= (FLT_EPSILON - 0.0001)))
            {
                Vec3f bc_clip(st, s, t);
                bc_clip = bc_clip / (bc_clip.x + bc_clip.y + bc_clip.z);
                float frag_depth = Vec3f(t0.z, t1.z, t2.z) * bc_clip;

                if (m_zbuffer->pixel(x, y) < frag_depth) {
                    m_zbuffer->setPixel(x, y, frag_depth);

                    float u = Vec3f(tex0.x, tex1.x, tex2.x) * bc_clip;
                    float v = Vec3f(tex0.y, tex1.y, tex2.y) * bc_clip;

                    Vec3f norm;
                    norm.x = Vec3f(norm0.x, norm1.x, norm2.x) * bc_clip;
                    norm.y = Vec3f(norm0.y, norm1.y, norm2.y) * bc_clip;
                    norm.z = Vec3f(norm0.z, norm1.z, norm2.z) * bc_clip;
                    //norm.normalize();

                    float intensity = norm * light_dir;
                    intensity = std::max(0.0f, std::min(1.0f, intensity));

                    Color c = texture2D(u, v);
                    c = c * intensity * (color / 255.0);

                    /*
                                        // Antialiasing
                                        if ((s + t <= 1.001) && (s + t > 0.99))
                                            //c = Color::RED;
                                            c = c / 1.5f;
                                        else if ((s >= -0.1) && (s < 0.01))
                                            //c = Color::GREEN;
                                            c = c / 1.5f;
                                        else if ((t >= -0.1) && (t < 0.01))
                                            //c = Color::BLUE;
                                            c = c / 1.5f;
                    */

                    m_framebuffer->setPixel(x, m_framebuffer->height() - y - 1, c);
                }
                notPainted = false;
            } else if (!notPainted) {
                break;
            }

            checkPixels++;
            x = x + stepX;
          //  x++;
        }
        y++;
        stepX *= -1;
        if (stepX == 1)
            x = minX;
        else
            x = maxX;
    }

   // qDebug() << checkPixels;
}
