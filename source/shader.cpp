#include "shader.h"

Vec4f IShader::texture2D(SamplerTexture2D &texture, float tu, float tv)
{
    if (texture.isNull())
        return Vec4f{1.0f, 1.0f, 1.0f, 1.0f};

    // Clamp
    tu = std::max(0.0f, std::min(tu, 1.0f));
    tv = std::max(0.0f, std::min(tv, 1.0f));

    if (m_enabledBilinearFilter)
    {
        tu *= (texture.width() - 1);
        tv *= (texture.height() - 1);
        int x = floor(tu);
        int y = floor(tv);

        float u_ratio = tu - x;
        float v_ratio = tv - y;
        float u_opposite = 1 - u_ratio;
        float v_opposite = 1 - v_ratio;

        Color tex_xy = QColorToColor(texture.pixel(x, y));
        Color tex_x1y = QColorToColor(texture.pixel(x + 1, y));
        Color tex_xy1 = QColorToColor(texture.pixel(x, y + 1));
        Color tex_x1y1 = QColorToColor(texture.pixel(x + 1, y + 1));

        Color result = (tex_xy * u_opposite + tex_x1y * u_ratio) * v_opposite +
                       (tex_xy1 * u_opposite + tex_x1y1 * u_ratio) * v_ratio;

        //return result;
        return Vec4f{result.r / 255.0f, result.g / 255.0f, result.b / 255.0f, 1.0f};
    } else {
        int u = tu * (texture.width() - 1);
        int v = tv * (texture.height() - 1);

        QRgb p = texture.pixel(u, v);
        return Vec4f{qBlue(p) / 255.0f, qGreen(p) / 255.0f, qRed(p) / 255.0f, 1.0f};
    }
}

float IShader::textureDepth(SamplerTextureDepth &texture, float tu, float tv)
{
    return texture.pixel(tu * (texture.width() - 1), tv * (texture.height() - 1));
}
