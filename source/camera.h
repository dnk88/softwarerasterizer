#ifndef CAMERA_H
#define CAMERA_H

#include "geometry.h"

class Camera
{
public:
    Camera();

    void setLookat(Vec3f eye, Vec3f center, Vec3f up);

    Matrix modelview() const
    {
        return m_modelview;
    }

private:

    Matrix m_modelview;
};

#endif // CAMERA_H
