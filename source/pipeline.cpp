#include "pipeline.h"

static Matrix viewport_m(int x, int y, int w, int h, int z) {
    Matrix Viewport = Matrix::identity();
    Viewport[0][3] = x + w / 2.f;
    Viewport[1][3] = y + h / 2.f;
    Viewport[2][3] = z / 2.0f;
    Viewport[0][0] = w / 2.f;
    Viewport[1][1] = h / 2.f;
    Viewport[2][2] = z / 2.0f;
    return Viewport;
}

static Matrix proj(float fov, float aspect) {
    Matrix projection = Matrix::identity();
    float f = 1 / tan((fov * 3.14159265 / 180) / 2);
    projection[0][0] = f / aspect;
    projection[1][1] = f;

    //projection[2][2] = (zFar + zNear) / (zNear - zFar);
    //projection[2][3] = (2 * zFar * zNear) / (zNear - zFar);
    //projection[3][3] = 0;

    projection[3][2] = -1.0f;
    return projection;
}


Pipeline::Pipeline()
{
    createFrameBuffer(800, 600);

    m_rasterizer = new Rasterizer(m_frameBuffer, m_depthBuffer);
    m_rasterizer->setEnableZBuffer(true);
}

Pipeline::~Pipeline()
{
    delete m_rasterizer;
    delete m_frameBuffer;
    delete m_depthBuffer;
    delete m_shader;
}

void Pipeline::setShader(IShader *shader)
{
    m_shader = shader;
}

void Pipeline::resizeFrameBuffer(int width, int height)
{
    delete m_frameBuffer;
    delete m_depthBuffer;

    createFrameBuffer(width, height);
    m_rasterizer->setFrameBuffer(m_frameBuffer, m_depthBuffer);
}

void Pipeline::createFrameBuffer(int width, int height)
{
    m_width = width;
    m_height = height;

    m_frameBuffer = new PixelBuffer(m_width, m_height);
    m_depthBuffer = new DepthBuffer(m_width, m_height);
}

void Pipeline::clearFramebuffer()
{
    assert(m_frameBuffer);
    assert(m_depthBuffer);

    m_frameBuffer->clear();
    m_depthBuffer->clear(std::numeric_limits<int>::min());
}

void Pipeline::setupShaderMatrixs(const Matrix &modelMatrix)
{
    assert(m_shader);

    const int depth = 1;
    Matrix viewport = viewport_m(0, 0, m_width, m_height, depth);

    m_shader->projectionMatrix = proj(90.0f, 1.0f * m_width / m_height);
    m_shader->viewMatrix = m_camera.modelview();
    m_shader->modelMatrix = modelMatrix;
    m_shader->viewportMatrix = viewport;
    m_shader->modelViewMatrix = m_shader->viewMatrix * m_shader->modelMatrix;
    m_shader->modelViewProjectionMatrix = m_shader->projectionMatrix * m_shader->modelViewMatrix;

    m_shader->setupUniformValues();
}

void Pipeline::resizeVaryingAttribs(TrianglePrimitive &primitive)
{
    assert(m_shader);

    // Resize varying float attribs
    primitive.varyingFloatAtrribs.resize(m_shader->varyingFloatAtrribs.size());
    for (size_t k = 0; k < m_shader->varyingFloatAtrribs.size(); k++)
        primitive.varyingFloatAtrribs.at(k).resize(3);

    // Resize varying vec3f attribs
    primitive.varyingVec3fAtrribs.resize(m_shader->varyingVec3fAtrribs.size());
    for (size_t k = 0; k < m_shader->varyingVec3fAtrribs.size(); k++)
        primitive.varyingVec3fAtrribs.at(k).resize(3);
}

void Pipeline::drawModel(const Model &model)
{
    assert(m_shader);
    assert(m_rasterizer);

    TrianglePrimitive primitive;
    Vec4f vertexColor{1.0f, 1.0f, 1.0f, 1.0f};

    setupShaderMatrixs(model.transform());
    resizeVaryingAttribs(primitive);

    // Processing model
    for (int i = 0; i < model.nfaces(); i++)
    {
        std::vector<int> face = model.face(i);

        for (int j = 0; j < 3; j++)
        {
            Vec4f v = embed<4>(model.vert(face[j]));

            m_shader->normal = embed<4>(model.normal(i, j), 0.0f);
            m_shader->texCoord = model.uv(i, j);

            // Call vertex shader
            m_shader->vertex(v);
            m_shader->position = m_shader->viewportMatrix * m_shader->position;

            // Assembly primitive
            primitive.normal[j] = proj<3>(m_shader->normal).normalize();
            primitive.tex[j] = Vec2f(m_shader->texCoord.x, m_shader->texCoord.y);
            primitive.color[j] = vertexColor;

            // Perspective division
            primitive.vert[j] = Vec3f(m_shader->position[0] / m_shader->position[3],
                                      m_shader->position[1] / m_shader->position[3],
                                      m_shader->position[2] / m_shader->position[3]);
            primitive.w[j] = m_shader->position[3];

            for (size_t k = 0; k < m_shader->varyingFloatAtrribs.size(); k++)
                primitive.varyingFloatAtrribs[k][j] = *m_shader->varyingFloatAtrribs.at(k);

            for (size_t k = 0; k < m_shader->varyingVec3fAtrribs.size(); k++)
                primitive.varyingVec3fAtrribs[k][j] = *m_shader->varyingVec3fAtrribs.at(k);
        }

        // Rasterize primitive
        m_rasterizer->triangle(primitive, *m_shader);
    }
}
