#ifndef PIXELBUFFER_H
#define PIXELBUFFER_H

#include <cstdint>
#include <cstring>
#include <QColor>
#include <QImage>
#include <QPainter>
#include "geometry.h"

class ColorRGBA32 final
{
public:
   unsigned char r, g, b, a;

    ColorRGBA32 operator*(float const s) const
    {
        return ColorRGBA32{static_cast<unsigned char>(this->r * s),
                           static_cast<unsigned char>(this->g * s),
                           static_cast<unsigned char>(this->b * s),
                           static_cast<unsigned char>(this->a * s)};
    }

    ColorRGBA32 operator*(ColorRGBA32 const &another) const
    {
        return ColorRGBA32{static_cast<unsigned char>(this->r * another.r),
                           static_cast<unsigned char>(this->g * another.g),
                           static_cast<unsigned char>(this->b * another.b),
                           static_cast<unsigned char>(this->a * another.a)};
    }

    ColorRGBA32 operator/(float const s) const
    {
        return ColorRGBA32{static_cast<unsigned char>(this->r / s),
                           static_cast<unsigned char>(this->g / s),
                           static_cast<unsigned char>(this->b / s),
                           static_cast<unsigned char>(this->a / s)};
    }

    ColorRGBA32 operator+(ColorRGBA32 const &another) const
    {
        return ColorRGBA32{static_cast<unsigned char>(this->r + another.r),
                           static_cast<unsigned char>(this->g + another.g),
                           static_cast<unsigned char>(this->b + another.b),
                           static_cast<unsigned char>(this->a + another.a)};
    }

    static const ColorRGBA32 BLACK;
    static const ColorRGBA32 WHITE;
    static const ColorRGBA32 RED;
    static const ColorRGBA32 GREEN;
    static const ColorRGBA32 BLUE;
};

template<typename T>
class TPixelBuffer
{
private:
    unsigned int m_width, m_height;
    unsigned int m_pitch;

    T *m_buffer;
public:
    TPixelBuffer(const unsigned int w, const unsigned int h)
        : m_width(w),
          m_height(h),
          m_pitch(w * sizeof(T)),
          m_buffer(nullptr)
   {
       m_buffer = new T[w * h];
       clear();
   }

    ~TPixelBuffer()
    {
        if (m_buffer != nullptr)
            delete[] m_buffer;
    }

    TPixelBuffer<T> *clone()
    {
        TPixelBuffer<T> *newClone = new TPixelBuffer<T>(m_width, m_height);
        std::memcpy(reinterpret_cast<unsigned char *>(newClone->m_buffer),
                    this->rawData(), m_width * m_height * sizeof(T));
        return newClone;
    }

    void copy(TPixelBuffer<T> *src)
    {
        std::memcpy(this->rawData(), src->rawData(), m_width * m_height * sizeof(T));
    }

    void setPixel(const unsigned int x, const unsigned int y, T c)
    {
        if ((x >= m_width) || (y >= m_height))
            return;
        m_buffer[m_width * y + x] = c;
    }

    T pixel(const unsigned int x, const unsigned int y) const
    {
        if ((x >= m_width) || (y >= m_height))
            return T();
        return m_buffer[m_width * y + x];
    }

    void clear()
    {
        std::memset(m_buffer, 0, m_width * m_height * sizeof(T));
    }

    void clear(int v)
    {
        std::memset(m_buffer, v, m_width * m_height * sizeof(T));
    }

    unsigned int width() const
    {
        return m_width;
    }

    unsigned int height() const
    {
        return m_height;
    }

    unsigned char const *rawData() const
    {
        return reinterpret_cast<unsigned char *>(m_buffer);
    }
};

typedef ColorRGBA32 Color;
typedef TPixelBuffer<Color> PixelBuffer;
typedef TPixelBuffer<float> DepthBuffer;

Color QColorToColor(QColor c);
Color Vec4fToColor(Vec4f v);
Vec4f ColorToVec4f(Color c);

#endif // PIXELBUFFER_H
