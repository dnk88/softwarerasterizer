#include "rasterizer.h"
#include <QDebug>
#include <QApplication>
#include <QRgb>
#include <algorithm>
#include <vector>
#include <cmath>
#include <limits>
#include <cfloat>

Rasterizer::Rasterizer(PixelBuffer *frameBuffer, DepthBuffer *depthBuffer):
    m_framebuffer(nullptr),
    m_zbuffer(nullptr),
    m_scissorBox(0, 0, 0, 0),
    m_renderMode(RenderMode::SolidMode),
    m_enabledZBuffer(false),
    m_backCulling(true),
    m_blending(false)
{
    setFrameBuffer(frameBuffer, depthBuffer);
}

Rasterizer::~Rasterizer()
{
}

void Rasterizer::setFrameBuffer(PixelBuffer *frameBuffer, DepthBuffer *depthBuffer)
{
  assert(frameBuffer);
  assert(depthBuffer);

  m_framebuffer = frameBuffer,
  m_zbuffer = depthBuffer,

  m_scissorBox.setWidth(m_framebuffer->width());
  m_scissorBox.setHeight(m_framebuffer->height());
}

void Rasterizer::setEnableZBuffer(bool enabled)
{
    m_enabledZBuffer = enabled;
}

bool Rasterizer::enabledZBuffer() const
{
    return m_enabledZBuffer;
}

void Rasterizer::setEnableBlending(bool enabled)
{
    m_blending = enabled;
}

bool Rasterizer::enabledBlending() const
{
    return m_blending;
}

void Rasterizer::setRenderMode(RenderMode mode)
{
    m_renderMode = mode;
}

RenderMode Rasterizer::renderMode() const
{
    return m_renderMode;
}

DepthBuffer *Rasterizer::depthBuffer()
{
    return m_zbuffer;
}

void Rasterizer::InterpolateVaryingAttribs(Vec3f bcScreen, const TrianglePrimitive &tri, IShader &shader)
{
    // Interpolate varying float attribs
    for (size_t k = 0; k < shader.varyingFloatAtrribs.size(); k++)
    {
        const float &a0 = tri.varyingFloatAtrribs[k][0];
        const float &a1 = tri.varyingFloatAtrribs[k][1];
        const float &a2 = tri.varyingFloatAtrribs[k][2];

        *shader.varyingFloatAtrribs.at(k) = Vec3f(a0, a1, a2) * bcScreen;
    }

    // Interpolate varying Vec3f attribs
    for (size_t k = 0; k < shader.varyingVec3fAtrribs.size(); k++)
    {
        // TODO: replace to matrix
        Vec3f varyingAtrrib;
        const Vec3f &a0 = tri.varyingVec3fAtrribs[k][0];
        const Vec3f &a1 = tri.varyingVec3fAtrribs[k][1];
        const Vec3f &a2 = tri.varyingVec3fAtrribs[k][2];

        varyingAtrrib.x = Vec3f(a0.x, a1.x, a2.x) * bcScreen;
        varyingAtrrib.y = Vec3f(a0.y, a1.y, a2.y) * bcScreen;
        varyingAtrrib.z = Vec3f(a0.z, a1.z, a2.z) * bcScreen;

        *shader.varyingVec3fAtrribs.at(k) = varyingAtrrib;
    }
}

void Rasterizer::processFragment(Vec4f fragColor, IShader &shader)
{
    if (shader.fragment(fragColor))
    {
        unsigned int x = shader.fragCoord.x;
        unsigned int y = shader.fragCoord.y;

        // Write depth value to z buffer
        m_zbuffer->setPixel(x, y, shader.fragDepth);

        // Blending
        if (m_blending)
        {
            Color colorDst = m_framebuffer->pixel(x, m_framebuffer->height() - y - 1);
            shader.fragColor = blend(shader.fragColor, ColorToVec4f(colorDst), 1, 1, 1, true);
        }

        m_framebuffer->setPixel(x, m_framebuffer->height() - y - 1, Vec4fToColor(shader.fragColor));
    }
}

Vec4f Rasterizer::blend(Vec4f src, Vec4f dst, int x, int y, int z, bool useSrc)
{
    Vec4f result;
    if (useSrc)
        result = src;
    else
        result = dst;

    result = result * (src[3] * dst[3]);
    result = result + (src * src[3] * (y * (1 - dst[3])));
    result = result + (dst * dst[3] * (z * (1 - src[3])));

    result[3] = x * src[3] * dst[3];
    result[3] = result[3] + src[3] * y * (1 - dst[3]);
    result[3] = result[3] + dst[3] * z * (1 - src[3]);

    return result;
}

void Rasterizer::triangle(const TrianglePrimitive &tri, IShader &shader)
{
    if (tri.vert[0].y == tri.vert[1].y && tri.vert[0].y == tri.vert[2].y)
        return; // i dont care about degenerate triangles

    // get the bounding box of the triangle
    int maxX = std::max(tri.vert[0].x, std::max(tri.vert[1].x, tri.vert[2].x));
    int minX = std::min(tri.vert[0].x, std::min(tri.vert[1].x, tri.vert[2].x));
    int maxY = std::max(tri.vert[0].y, std::max(tri.vert[1].y, tri.vert[2].y));
    int minY = std::min(tri.vert[0].y, std::min(tri.vert[1].y, tri.vert[2].y));

    // spanning vectors of edge (v1,v2) and (v1,v3)
    Vec2f vs1(tri.vert[1].x - tri.vert[0].x, tri.vert[1].y - tri.vert[0].y);
    Vec2f vs2(tri.vert[2].x - tri.vert[0].x, tri.vert[2].y - tri.vert[0].y);

    // Модуль векторного произведения двух краев треугольника (определитель матрицы)
    float cs_vs12 = dot(vs1, vs2);
    shader.frontFacing = true;
    if (cs_vs12 <= 1)
    {
        shader.frontFacing = false;

        // back side triangle skip
        if (m_backCulling)
            return;
    }

    maxX = std::min(maxX, m_scissorBox.right());
    maxY = std::min(maxY, m_scissorBox.bottom());
    minX = std::max(minX, m_scissorBox.left());
    minY = std::max(minY, m_scissorBox.top());

    for (int x = minX; x <= maxX; x++)
    {
        for (int y = minY; y <= maxY; y++)
        {
            // Определяем находится ли точка внутри треугольника
            Vec2f q(x - tri.vert[0].x, y - tri.vert[0].y);

            float s = dot(q, vs2) / cs_vs12;
            float t = dot(vs1, q) / cs_vs12;
            float st = (1.f - (s + t));

            // Inside triangle
            if ((s >= 0) && (t >= 0) && (s + t <= 1.0001))
            {
                Vec3f bcScreen(st, s, t);

                Vec3f bcClip(st / tri.w[0], s / tri.w[1], t / tri.w[2]);
                bcClip = bcClip / (bcClip.x + bcClip.y + bcClip.z);

                float fragDepth = Vec3f(tri.vert[0].z, tri.vert[1].z, tri.vert[2].z) * bcScreen;

                // Test z buffer
                if (m_zbuffer->pixel(x, y) < fragDepth)
                {
                    // Inteprolate texture coords
                    float u = Vec3f(tri.tex[0].x, tri.tex[1].x, tri.tex[2].x) * bcClip;
                    float v = Vec3f(tri.tex[0].y, tri.tex[1].y, tri.tex[2].y) * bcClip;

                    // Interpolate color
                    Vec4f fragColor;
                    fragColor[0] = Vec3f(tri.color[0][0], tri.color[0][1], tri.color[0][2]) * bcScreen;
                    fragColor[1] = Vec3f(tri.color[1][0], tri.color[1][1], tri.color[1][2]) * bcScreen;
                    fragColor[2] = Vec3f(tri.color[2][0], tri.color[2][1], tri.color[2][2]) * bcScreen;
                    fragColor[3] = Vec3f(tri.color[3][0], tri.color[3][1], tri.color[3][2]) * bcScreen;

                    // Interpolate normals
                    Vec3f norm;
                    // TODO: replace to matrix
                    norm.x = Vec3f(tri.normal[0].x, tri.normal[1].x, tri.normal[2].x) * bcScreen;
                    norm.y = Vec3f(tri.normal[0].y, tri.normal[1].y, tri.normal[2].y) * bcScreen;
                    norm.z = Vec3f(tri.normal[0].z, tri.normal[1].z, tri.normal[2].z) * bcScreen;

                    // Interpolate frag coordinates
                    Vec3f fragCoord;
                    // TODO: replace to matrix
                    fragCoord.x = x;
                    fragCoord.y = y;
                    fragCoord.z = fragDepth;

                    // Processing fragment shaders
                    shader.fragDepth = fragDepth;
                    shader.texCoord = Vec3f(u, v, 1.0f);
                    shader.fragNormal = norm;
                    shader.fragCoord = fragCoord;

                    InterpolateVaryingAttribs(bcScreen, tri, shader);

                    processFragment(fragColor, shader);
                }
            }
        }
    }
}
