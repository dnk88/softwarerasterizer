#include "camera.h"

Camera::Camera()
{
}

void Camera::setLookat(Vec3f eye, Vec3f center, Vec3f up)
{
        Vec3f z = (eye - center).normalize();
        Vec3f x = (up ^ z).normalize();
        Vec3f y = (z ^ x).normalize();
        Matrix Minv = Matrix::identity();
        Matrix Tr   = Matrix::identity();
        for (int i = 0; i < 3; i++) {
            Minv[0][i] = x[i];
            Minv[1][i] = y[i];
            Minv[2][i] = z[i];
            Tr[i][3] = -center[i];
        }
        m_modelview = Minv * Tr;
}
