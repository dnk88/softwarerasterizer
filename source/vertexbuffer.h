#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H

#include <vector>
#include "geometry.h"

struct VertexBuffer
{
    std::vector<Vec3f> verts;
    std::vector<Vec3f> colors;
    std::vector<Vec3f> normals;
    std::vector<Vec2f> texs0;
};

#endif // VERTEXBUFFER_H
