#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "shader.h"
#include "lighting.h"

namespace Ui {
class MainWindow;
}

class Pipeline;
class Model;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void paintImage();
    void paintWireHead();
    void changeShader();
    void changeShaderSettings();

private:
    Ui::MainWindow *ui;

    LamberLightShader *shader1;
    BlinnLightShader *shader2;
    BumpMapPhongLightShader *shader3;
    DepthShader *shader4;
    PhongLightShader *shader5;

    Pipeline *pipeline;
    Model *floor;
    Model *model;
    QImage *tex;
    QImage *tex2;
    QImage *tex3;
};

#endif // MAINWINDOW_H
